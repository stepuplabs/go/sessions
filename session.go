package sessions

import (
	"bytes"
	"encoding/gob"
	"errors"
	"fmt"
	"strings"
	"time"

	"gitlab.com/stepuplabs/go/crypt"
)

/*
 * --- HTTPSession ---
 */

// HTTPSession represents a http session
type HTTPSession struct {
	ID         string
	Values     map[string][]byte
	CreatedAt  time.Time
	LastAccess time.Time
	ExpiresAt  time.Time

	sm HTTPSessionManager
}

// Set - set value
func (session *HTTPSession) Set(key string, value interface{}) {
	var buff bytes.Buffer

	if session.Values == nil {
		session.Values = make(map[string][]byte)
	}

	gobEncoder := gob.NewEncoder(&buff)

	err := gobEncoder.Encode(value)

	if err != nil {
		return
	}

	digestedKey := crypt.MD5([]byte(key))

	session.Values[digestedKey] = buff.Bytes()
}

// Get - get value
func (session *HTTPSession) Get(key string, value interface{}) (err error) {

	if session.Values == nil {
		session.Values = make(map[string][]byte)
	}

	digestedKey := crypt.MD5([]byte(key))

	encodedObject, keyFound := session.Values[digestedKey]

	if !keyFound {
		err = fmt.Errorf("Key not found: %s", digestedKey)
	}

	reader := bytes.NewBuffer(encodedObject)

	gobDecoder := gob.NewDecoder(reader)

	err = gobDecoder.Decode(value)

	return
}

// GetInt - get value as integer
func (session *HTTPSession) GetInt(key string) (value int64, err error) {

	err = session.Get(key, &value)

	if err != nil {
		return
	}

	return
}

// GetString - get value as string
func (session *HTTPSession) GetString(key string) (value string, err error) {

	err = session.Get(key, &value)

	if err != nil {
		return
	}

	return
}

// GetBool - get value as boolean
func (session *HTTPSession) GetBool(key string) (value bool, err error) {

	err = session.Get(key, &value)

	if err != nil {
		return
	}

	return
}

// GetFloat - get value as float64
func (session *HTTPSession) GetFloat(key string) (value float64, err error) {

	err = session.Get(key, &value)

	if err != nil {
		return
	}

	return
}

// Serialize - convert session values into a single string
func (session *HTTPSession) Serialize() (result string) {

	result = ""

	for key, value := range session.Values {
		result += fmt.Sprintf("%s:%s;", key, crypt.Base64Encode(value))
	}

	return
}

// Deserialize - load session values from a serialized string
func (session *HTTPSession) Deserialize(content string) (err error) {

	if session.Values == nil {
		session.Values = make(map[string][]byte)
	}

	variables := strings.Split(content, ";")

	for _, v := range variables {

		pair := strings.Split(v, ":")

		if len(pair) != 2 {
			continue
		}

		value, err := crypt.Base64Decode(pair[1])

		if err != nil {
			continue
		}

		session.Values[pair[0]] = value
	}

	return
}

// Save stores session using session manager
func (session *HTTPSession) Save() error {

	if session.sm == nil {
		return errors.New("invalid reference of session manager")
	}

	return session.sm.SaveSession(session)
}

// Destroy cleanup session using session manager
func (session *HTTPSession) Destroy() error {

	sID := session.ID

	if session.sm == nil {
		return errors.New("invalid reference of session manager")
	}

	return session.sm.DestroySession(sID)
}
