module gitlab.com/stepuplabs/go/sessions

go 1.13

require (
	github.com/go-redis/redis v6.15.6+incompatible
	gitlab.com/stepuplabs/go/config v1.0.0
	gitlab.com/stepuplabs/go/crypt v1.0.1
	gitlab.com/stepuplabs/go/rainforest v1.1.0
)
