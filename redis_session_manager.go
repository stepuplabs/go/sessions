package sessions

import (
	"fmt"
	"time"

	"github.com/go-redis/redis"
	"gitlab.com/stepuplabs/go/config"
	"gitlab.com/stepuplabs/go/crypt"
)

// RedisSessionManager implements HTTPSessionManager interface
type RedisSessionManager struct {
	Address   string
	Password  string
	SIDPrefix string

	client *redis.Client
}

// Initialize session manager
func (sm *RedisSessionManager) Initialize(cs *config.Set) (err error) {

	sm.Address, _ = cs.GetString("session_manager.redis.address")
	sm.Password, _ = cs.GetString("session_manager.redis.password")

	if len(sm.SIDPrefix) == 0 {
		sm.SIDPrefix = "SID"
	}

	sm.client = redis.NewClient(&redis.Options{
		Addr:     sm.Address,
		Password: sm.Password,
		DB:       0,
	})

	_, err = sm.client.Ping().Result()

	if err != nil {
		return
	}

	return
}

// CreateSession creates a new session
func (sm *RedisSessionManager) CreateSession() (session HTTPSession, err error) {

	duration := time.Hour * 24 * 30

	session = HTTPSession{
		ID:         fmt.Sprintf("%s-%s", sm.SIDPrefix, crypt.UUID()),
		CreatedAt:  time.Now(),
		LastAccess: time.Now(),
		ExpiresAt:  time.Now().Add(duration),
		sm:         sm, /* Reference of SessionManager */
	}

	err = sm.SaveSession(&session)

	return
}

// LoadSession loads session by sID from redis database
func (sm *RedisSessionManager) LoadSession(sID string) (session HTTPSession, err error) {

	var serialized string

	serialized, err = sm.client.Get(sID).Result()

	if err != nil {
		return
	}

	session = HTTPSession{ID: sID, Values: make(map[string][]byte)}

	err = session.Deserialize(serialized)

	if err != nil {
		return
	}

	session.sm = sm /* Reference of SessionManager */

	return
}

// SaveSession stores session in redis database
func (sm *RedisSessionManager) SaveSession(session *HTTPSession) (err error) {

	err = sm.client.Set(session.ID, session.Serialize(), time.Hour*24*30).Err()

	return
}

// DestroySession if exists
func (sm *RedisSessionManager) DestroySession(sID string) error {
	return sm.client.Del(sID).Err()
}

// Terminate session manager
func (sm *RedisSessionManager) Terminate() {
	sm.client.Close()
}
