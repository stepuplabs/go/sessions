package sessions

import (
	"os"
	"testing"

	"gitlab.com/stepuplabs/go/config"
)

type Person struct {
	Name string
	Age  int
}

func doSessionManagerTest(sm HTTPSessionManager, t *testing.T) {

	t.Logf("Initializing Session Manager...")

	cs, err := config.Load("config.yml")

	if err != nil {
		t.Error(err)
		return
	}

	err = sm.Initialize(cs)

	if err != nil {
		t.Error(err)
		return
	}

	session, err := sm.CreateSession()

	if err != nil {
		t.Error(err)
		return
	}

	person := Person{
		Name: "Diego",
		Age:  34,
	}

	session.Set("ival", 6379)
	session.Set("sval", "string-value")
	session.Set("bval", true)
	session.Set("fval", 3.4)
	session.Set("oval", person)

	err = session.Save()

	if err != nil {
		t.Error(err)
		return
	}

	loadedSession, err := sm.LoadSession(session.ID)

	if err != nil {
		t.Error(err)
		return
	}

	ival, _ := loadedSession.GetInt("ival")

	if ival != 6379 {
		t.Errorf("invalid ival: %d", ival)
		return
	}

	sval, _ := loadedSession.GetString("sval")

	if sval != "string-value" {
		t.Errorf("invalid sval: %s", sval)
		return
	}

	bval, _ := loadedSession.GetBool("bval")

	if !bval {
		t.Errorf("invalid bval: %v", bval)
		return
	}

	fval, _ := loadedSession.GetFloat("fval")

	if fval != 3.4 {
		t.Errorf("invalid fval: %f", fval)
		return
	}

	loadedPerson := Person{}

	err = loadedSession.Get("oval", &loadedPerson)

	if err != nil {
		t.Error(err)
		return
	}

	if loadedPerson.Name != "Diego" || loadedPerson.Age != 34 {
		t.Errorf("invalid oval: %v", loadedPerson)
		return
	}

	loadedSession.Destroy()

	_, err = sm.LoadSession(loadedSession.ID)

	if err == nil {
		t.Errorf("session.Destroy did not deleted session from redis database")
	}
}

func TestRedisSessionManager(t *testing.T) {

	sm := RedisSessionManager{
		Address:   "localhost:6379",
		Password:  "",
		SIDPrefix: "Test",
	}

	doSessionManagerTest(&sm, t)
}

func TestFSSessionManager(t *testing.T) {

	sm := FSSessionManager{
		SessionDir: ".test-sessions",
		SIDPrefix:  "Test",
	}

	doSessionManagerTest(&sm, t)

	os.Remove(sm.SessionDir)
}
