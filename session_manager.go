package sessions

import "gitlab.com/stepuplabs/go/config"

/*
 * --- HTTPSessionManager Interface ---
 */

// HTTPSessionManager describes a basci session manager
type HTTPSessionManager interface {

	// initialize session manager
	Initialize(cs *config.Set) (err error)

	// create a new session
	CreateSession() (session HTTPSession, err error)

	// load session by key if exists
	LoadSession(sID string) (session HTTPSession, err error)

	// save session
	SaveSession(session *HTTPSession) (err error)

	// destroy session if exists
	DestroySession(sID string) (err error)

	// terminate session manager

	Terminate()
}
