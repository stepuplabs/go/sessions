package sessions

import (
	"fmt"
	"io/ioutil"
	"os"
	"time"

	"gitlab.com/stepuplabs/go/config"
	"gitlab.com/stepuplabs/go/crypt"
)

// FSSessionManager implements HTTPSessionManager storing sessions in the filesystem
type FSSessionManager struct {
	SessionDir string
	SIDPrefix  string
}

// Initialize checks if SessionDir exists, if not exists, create it
func (sm *FSSessionManager) Initialize(cs *config.Set) (err error) {

	var dStat os.FileInfo

	sm.SessionDir, _ = cs.GetString("session_manager.fs.session_dir")
	sm.SIDPrefix, _ = cs.GetString("session_manager.fs.sid_prefix")

	if len(sm.SessionDir) == 0 {
		sm.SessionDir = ".sessions"
	}

	if len(sm.SIDPrefix) == 0 {
		sm.SIDPrefix = "SID"
	}

	dStat, err = os.Stat(sm.SessionDir)

	if err == nil && dStat.IsDir() {
		return
	}

	err = os.Mkdir(sm.SessionDir, os.ModePerm)

	return
}

// CreateSession creates a new session and save it in filesystem
func (sm *FSSessionManager) CreateSession() (session HTTPSession, err error) {

	duration := time.Hour * 24

	session = HTTPSession{
		ID:         fmt.Sprintf("%s-%s", sm.SIDPrefix, crypt.UUID()),
		CreatedAt:  time.Now(),
		LastAccess: time.Now(),
		ExpiresAt:  time.Now().Add(duration),
		sm:         sm, // reference of session manager that created this session
	}

	return
}

// LoadSession loads a session by sessionID if exists
func (sm *FSSessionManager) LoadSession(sID string) (session HTTPSession, err error) {

	var fStat os.FileInfo

	path := fmt.Sprintf("%s/%s", sm.SessionDir, sID)

	fStat, err = os.Stat(path)

	if err != nil {
		return
	}

	session = HTTPSession{
		ID:         sID,
		CreatedAt:  fStat.ModTime(),
		LastAccess: time.Now(),
		ExpiresAt:  time.Now().Add(time.Hour * 24),
	}

	sFile, err := os.Open(path)

	if err != nil {
		return
	}
	defer sFile.Close()

	content, e := ioutil.ReadAll(sFile)

	if e != nil {
		err = e
		return
	}

	err = session.Deserialize(string(content))

	if err != nil {
		return
	}

	session.sm = sm // reference of session manager that loaded this session

	return
}

// SaveSession stores session in filesystem
func (sm *FSSessionManager) SaveSession(session *HTTPSession) (err error) {

	path := fmt.Sprintf("%s/%s", sm.SessionDir, session.ID)

	session.LastAccess = time.Now()
	session.ExpiresAt = session.LastAccess.Add(time.Hour * 24)

	sFile, err := os.Create(path)

	if err != nil {
		return
	}
	defer sFile.Close()

	_, err = sFile.WriteString(session.Serialize())

	if err != nil {
		return
	}

	return
}

// DestroySession cleanup
func (sm *FSSessionManager) DestroySession(sID string) error {
	return os.Remove(fmt.Sprintf("%s/%s", sm.SessionDir, sID))
}

// Terminate session manager
func (sm *FSSessionManager) Terminate() {
	// nothing to do
}
